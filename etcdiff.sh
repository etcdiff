#!/bin/sh
#
# etcdiff (deb-etcdiff?) shows how your current /etc dir
# diverges from the debian default one.
#
# Copyright (C) 2008,2009,2010 Antonio Ospite <ospite@studenti.unina.it>
# License: GPLv2 or later
#
#set -x
#set -e

#PROMPT_RM=-i

DEBIANMIRROR="http://ftp.it.debian.org/debian"

BASEDIR=$(dirname $0)
TEMPDIR=${BASEDIR}/temp
CACHEDIR=${BASEDIR}/cache
REPORTDIR=${BASEDIR}/reports

usage()
{
  echo "usage: $0 <file|package|system> [<file name>|<package name>]

etcdiff shows how your /etc dir differs from the debian default one

etcdiff by explicit file list:
    FILES='/etc/sysctl.conf /etc/updatedb.conf'
    for file in \$FILES;
    do
      $0 file \$file
    done

etcdiff bycommand generated file list
    FILES=\$(find /etc/apache2 -type f -perm /o+r | grep -v '.dpkg-')
    for file in \$FILES;
    do
      $0 file \$file
    done

etcdiff by package name
    $0 package cherokee
    $0 package mlocate
    $0 package apache2-doc
    $0 package apache2.2-common
    $0 package libapache2-mod-php5
    $0 package hostapd

etcdiff the whole /etc system directory
    $0 system
"
}

rm -rf $PROMPT_RM $TEMPDIR   && mkdir $TEMPDIR
rm -rf $PROMPT_RM $CACHEDIR  && mkdir $CACHEDIR
rm -rf $PROMPT_RM $REPORTDIR && mkdir $REPORTDIR

. $BASEDIR/etcdiff.include

case $1 in
  file)
    etcdiff_by_file $2
    ;;
  package)
    etcdiff_by_package $2
    ;;
  system)
    etcdiff_system
    ;;
  *)
    usage
    ;;
esac
